# social force model

Social Force ModelのRust実装とWASMで実装したGUIエミュレーター

## 20220816

wasmで実装した（巻き戻しの実装はできてない）

![](demo-wasm01.gif)

![](demo-wasm02.gif)

![](demo-wasm03.gif)

```
wasm-pack build
cd www
npm install 
npm start
```

## 20220814

![](./1660640090.gif)

```sh
cargo test test::test_simulation
```

> - http://vision.cse.psu.edu/courses/Tracking/vlpr12/HelbingSocialForceModel95.pdf
> - https://www.apptec.co.jp/technical_report/pdf/vol27/treport_vol_27-05.pdf

## 20220812

![](./1660234377.png)

```sh
cargo test model::node::test::test_move
```
