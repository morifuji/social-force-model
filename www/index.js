import { init, Position, Node, Wall, World, Goal } from "wasm-social-force-model";

init()

const canvas = document.querySelector("#canvas")
canvas.width=500
canvas.height=500

const OUTER_WALLS = [
    Wall.new(
        Position.new(0.0, 0.0),
        Position.new(0.0, 20.0)
    ),
    Wall.new(
        Position.new(0.0, 20.0),
        Position.new(20.0, 20.0)
    ),
    Wall.new(
        Position.new(20.0, 20.0),
        Position.new(20.0, 0.0)
    ),
    Wall.new(
        Position.new(20.0, 0.0),
        Position.new(0.0, 0.0)
    )
]

let time = 0

// PATTERN1. 
// const nodeList = [
//     Node.new(Position.new(0.0, 1.0), Position.new(19, 19)),
//     Node.new(Position.new(2.0, 3.0), Position.new(19, 19)),
//     Node.new(Position.new(4.0, 2.0), Position.new(19, 19)),
// ]
// const wallList = [
//     Wall.new(
//         Position.new(0.0, 0.0),
//         Position.new(0.0, 20.0)
//     ),
//     Wall.new(
//         Position.new(0.0, 20.0),
//         Position.new(20.0, 20.0)
//     ),
//     Wall.new(
//         Position.new(20.0, 20.0),
//         Position.new(20.0, 0.0)
//     ),
//     Wall.new(
//         Position.new(20.0, 0.0),
//         Position.new(0.0, 0.0)
//     ),
//     Wall.new(
//         Position.new(0.0, 5.0),
//         Position.new(15.0, 5.0)
//     ),
//     Wall.new(Position.new(10.0, 0.0),
//         Position.new(10.0, 3.0)
//     )
// ]

// PATTERN2.
// const nodeList = [
//     // NOTE 上下左右に対照な配置座標だと拮抗して移動が止まる、なので配置座標に揺らぎをあたえる必要がある
//     Node.new(Position.new(1.0, 1.0), Position.new(18, 18)),
//     Node.new(Position.new(1.0+0.1, 2.0), Position.new(18, 18)),
//     Node.new(Position.new(2.0, 1.0+0.1), Position.new(18, 18)),
//     Node.new(Position.new(2.0, 2.0), Position.new(18, 18)),

//     Node.new(Position.new(18.0, 1.0+0.1), Position.new(1, 18)),
//     Node.new(Position.new(17.0, 1.0+0.1), Position.new(1, 18)),
//     Node.new(Position.new(18.0, 2.0+0.1), Position.new(1, 18)),
//     Node.new(Position.new(17.0+0.1, 2.0), Position.new(1, 18)),

//     Node.new(Position.new(18.0+0.1, 18.0), Position.new(1, 1)),
//     Node.new(Position.new(17.0, 18.0+0.1), Position.new(1, 1)),
//     Node.new(Position.new(18.0, 17.0), Position.new(1, 1)),
//     Node.new(Position.new(17.0+0.1, 17.0), Position.new(1, 1)),

//     Node.new(Position.new(1.0, 18.0), Position.new(18, 1)),
//     Node.new(Position.new(2.0, 18.0+0.1), Position.new(18, 1)),
//     Node.new(Position.new(1.0, 17.0), Position.new(18, 1)),
//     Node.new(Position.new(2.0, 17.0), Position.new(18, 1)),
// ]
// const wallList = [
//     Wall.new(
//         Position.new(0.0, 0.0),
//         Position.new(0.0, 20.0)
//     ),
//     Wall.new(
//         Position.new(0.0, 20.0),
//         Position.new(20.0, 20.0)
//     ),
//     Wall.new(
//         Position.new(20.0, 20.0),
//         Position.new(20.0, 0.0)
//     ),
//     Wall.new(
//         Position.new(20.0, 0.0),
//         Position.new(0.0, 0.0)
//     ),
// ]

// PATTERN3
const nodeList = [...Array(100).keys()].map((_, i) => {
    return Node.new(Position.new(1.0 + (i%18), 1.0+ (Math.floor(i/18))), Goal.new(Position.new(10, 11), 0.5))
});

const wallList = [
    ...OUTER_WALLS,
    Wall.new(
        Position.new(0.0, 10.0),
        Position.new(9.5, 10.0),
    ),
    Wall.new(
        Position.new(20.0, 10.0),
        Position.new(10.5, 10.0),
    ),
    Wall.new(
        Position.new(10.5, 10.0),
        Position.new(10.5, 20.0),
    ),
    Wall.new(
        Position.new(9.5, 10.0),
        Position.new(9.5, 20.0),
    ),
]

let world = World.new_wasm(nodeList, wallList, 20, 20)

// nodeの履歴を記録する
const nodeHistories = []

const scaleHeight = canvas.height/world.height
const scaleWidth = canvas.width/world.width

const draw = () => {
    
    const ctx = canvas.getContext("2d");
    ctx.clearRect(0, 0, canvas.width, canvas.height);

    ctx.strokeStyle = "#000000";

    // 縮尺
    ctx.moveTo(1*scaleWidth, 19*scaleHeight);
    ctx.lineTo(2*scaleWidth, 19*scaleHeight);
    ctx.stroke();
    ctx.fillText("1m", 1*scaleWidth, 19.8*scaleHeight);

    // 経過時間
    ctx.fillText(`経過時間: ${time}s`, 1*scaleWidth, 1*scaleHeight);

    // LINE
    world._wall_list().forEach(wall=>{
        ctx.moveTo(wall.position_1.x*scaleWidth, wall.position_1.y*scaleHeight);
        ctx.lineTo(wall.position_2.x*scaleWidth, wall.position_2.y*scaleHeight);
        ctx.stroke();
    })

    // node
    world._node_list().forEach(node=>{
        ctx.strokeStyle = "#000000";
        ctx.beginPath();
        ctx.arc(node.position.x*scaleWidth, node.position.y*scaleHeight, node.r*scaleWidth, 0, 2 * Math.PI);
        ctx.stroke();

        // goal
        ctx.strokeStyle = "#c82124"
        ctx.beginPath();
        ctx.arc(node.goal.position.x*scaleWidth, node.goal.position.y*scaleHeight, node.goal.r*scaleHeight, 0, 2 * Math.PI);
        ctx.stroke();
    })
}

const saveHistory = () => {
    nodeHistories.push(world._node_list())
}
const restoreHistory = (historyIndex = 0) => {
    const nodeList = nodeHistories[historyIndex]
    world = World.new_wasm(nodeList, wallList, 20, 20)
}

const tick = (seconds) => {
    const simulationCount = seconds/0.1;
    [...new Array(simulationCount).keys()].forEach(()=>{
        world.go_by(0.1);
        saveHistory()
    })

    time += seconds
}

document.getElementById("next-0_1").addEventListener("click", () => {
    tick(0.1)
    draw()
})
document.getElementById("next-1").addEventListener("click", () => {
    tick(1)
    draw()
})
document.getElementById("next-10").addEventListener("click", () => {
    tick(10)
    draw()
})

document.getElementById("reset-0").addEventListener("click", () => {
    world = World.new_wasm(nodeList, wallList, 20, 20)
    draw()
})

// NOTE: nodeの履歴情報(nodeHistories)をwasm側に持たせていないため、jsで参照するとヌルポになる
// document.getElementById("reset-0_1").addEventListener("click", () => {
//     console.log(nodeHistories)
//     restoreHistory(nodeHistories.length -1 -1)
//     draw()
// })
// document.getElementById("reset-1").addEventListener("click", () => {
//     console.log(nodeHistories)
//     restoreHistory(nodeHistories.length -1 -10)
//     draw()
// })

draw()
saveHistory()
