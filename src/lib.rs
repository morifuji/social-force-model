mod graph;
mod model;

extern crate web_sys;

use wasm_bindgen::prelude::*;

#[wasm_bindgen]
extern "C" {
    fn alert(s: &str);
}

#[wasm_bindgen]
pub fn init() {
    console_error_panic_hook::set_once();
}

#[wasm_bindgen]
pub fn greet(s: &str) {
    alert(&format!("Hello, {}!", s));
}

mod test {

    use super::*;
    use crate::{graph::{draw_goal, draw_node, draw_wall}, model::goal::Goal};
    use chrono::Local;
    use model::{node::Node, world::World};
    use plotters::prelude::*;
    use model::{position::Position, wall::Wall};

    #[test]
    fn test_simulation() {
        let current_timestamp = Local::now().timestamp();

        let path = current_timestamp.to_string() + ".gif".as_ref();
        let root = BitMapBackend::gif(path, (1280, 1280), 100)
            .unwrap()
            .into_drawing_area();

        root.fill(&WHITE).unwrap();
        let mut chart = ChartBuilder::on(&root)
            .x_label_area_size(30)
            .y_label_area_size(30)
            .build_cartesian_2d(0.0f64..20.0f64, 0.0f64..20.0f64)
            .unwrap();

        chart.configure_mesh().draw().unwrap();

        let goal = Goal::new(Position::new(19.0, 19.0), 0.5);

        let node_list = vec![
            Node::new(Position::new(0.0, 1.0), goal),
            Node::new(Position::new(2.0, 3.0), goal),
            Node::new(Position::new(4.0, 2.0), goal),
        ];

        let wall_list = vec![
            Wall {
                position_1: Position::new(0.0, 0.0),
                position_2: Position::new(0.0, 20.0),
            },
            Wall {
                position_1: Position::new(0.0, 20.0),
                position_2: Position::new(20.0, 20.0),
            },
            Wall {
                position_1: Position::new(20.0, 20.0),
                position_2: Position::new(20.0, 0.0),
            },
            Wall {
                position_1: Position::new(20.0, 0.0),
                position_2: Position::new(0.0, 0.0),
            },
            Wall {
                position_1: Position::new(0.0, 5.0),
                position_2: Position::new(15.0, 5.0),
            },
            Wall {
                position_1: Position::new(10.0, 0.0),
                position_2: Position::new(10.0, 3.0),
            },
        ];

        let mut world = World::new(node_list, wall_list, 20, 20);

        draw_wall(&mut chart, world.wall_list());
        draw_goal(&mut chart, &goal);

        let span = 0.1;

        println!("[simulation] start");
        for turn_count in 0..500 {
            draw_node(&mut chart, world.node_list());
            root.present().expect("error in present");
            world.go_by(span);

            println!("[simulation] turn{}...", turn_count);
        }

        draw_node(&mut chart, world.node_list());
        root.present().expect("error in present");

        dbg!(world.node_list());

        println!("[simulation] end");
    }
}
