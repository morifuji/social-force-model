use std::f64::consts::E;

use super::_const::{INTERACTION_COEFFICIENT, RELAXATION_TIME, RESTITUTION_COEFFICIENT};
use super::position::Position;
use super::vector::Vector;
use super::goal::Goal;
use super::wall::Wall;
use serde::{Deserialize, Serialize};
use wasm_bindgen::prelude::*;

#[derive(Debug, Clone, Serialize, Deserialize)]
#[wasm_bindgen]
pub struct Node {
    pub position: Position,
    pub goal: Goal,

    pub weight: i32,

    /** 目標速度: 1 */
    pub target_velocity: f64,

    pub velocity: Vector,

    /** 半径 */
    pub r: f64,
}

impl Node {
    pub fn vect(&self) -> Vector {
        return self.goal.position - self.position;
    }

    pub fn acc(&self) -> Vector {
        // 理想速度
        let risou_velocity = self.vect().e() * self.target_velocity;
        // 理想-現実→加速度
        let acc = risou_velocity - self.velocity;

        acc
    }

    pub fn is_in_goal(&self) -> bool {
        let d = (self.position - self.goal.position).distance();
        d < self.goal.r
    }

    fn force_to_goal(&self) -> Vector {
        return self.acc() / RELAXATION_TIME;
    }

    fn force_from_wall(&self, wall_list: &Vec<Wall>) -> Vector {
        let mut force_all = Vector::new(0.0, 0.0);

        let force_list = wall_list.into_iter().map(|wall| {
            let d = wall.calc_distance_to_position_minimum(self.position);
            let _distance = self.r - d;
            if _distance > 5.0 {
                return Vector::new(0.0, 0.0);
            }
            let force_scholar =
                INTERACTION_COEFFICIENT * E.powf(_distance / RESTITUTION_COEFFICIENT);

            let orthogonal_vec = wall.min_vec(self.position);

            return orthogonal_vec.e() * force_scholar;
        });

        for force in force_list {
            force_all = force_all + force;
        }

        force_all
    }

    fn force_from_node(&self, node_list: &Vec<Node>) -> Vector {
        let force = node_list
            .into_iter()
            .map(|node| {
                let _distance = self.r - (node.position - self.position).distance();
                if _distance > 5.0 {
                    return Vector::new(0.0, 0.0);
                }
                let force_scholar =
                    INTERACTION_COEFFICIENT * E.powf(_distance / RESTITUTION_COEFFICIENT);
                (self.position - node.position).e() * force_scholar
            })
            .fold(Vector::new(0.0, 0.0), |acc, force| acc + force);

        force
    }

    pub fn _move(&mut self, duration: f64, wall_list: &Vec<Wall>, node_list: &Vec<Node>) {
        let f1 = self.force_to_goal();
        let f2 = self.force_from_wall(wall_list);
        let f3 = self.force_from_node(node_list);

        let mut new_velocity = self.velocity + ((f1 + f2 + f3) * duration);
        if new_velocity.distance() > self.target_velocity {
            new_velocity = (self.velocity + ((f1 + f2 + f3) * duration)).e() * self.target_velocity;
        }
        self.velocity = new_velocity;

        self.position = Position::new(
            (self.velocity.x * duration) + self.position.x,
            (self.velocity.y * duration) + self.position.y,
        );
    }
}

#[wasm_bindgen]
impl Node {
    pub fn _move_wasm(&mut self, duration: f64, _wall_list: &JsValue, _node_list: &JsValue) {
        // let wall_list: Vec<Wall> = serde_json::from_str(&_wall_list).unwrap();
        // let node_list: Vec<Node> = serde_json::from_str(&_node_list).unwrap();
        let wall_list: Vec<Wall> = _wall_list.into_serde().unwrap();
        let node_list: Vec<Node> = _node_list.into_serde().unwrap();

        self._move(duration, &wall_list, &node_list)
    }

    pub fn new(position: Position, goal: Goal) -> Self {
        Node {
            goal,
            // 半径
            r: 0.25,
            weight: 80,
            position,
            target_velocity: 1.0,
            velocity: Vector { x: 0.0, y: 0.0 },
        }
    }
}

#[cfg(test)]
mod test {
    use super::*;

    use crate::graph::plot;

    #[test]
    fn test_move_with_wall() {
        let mut node = Node::new(Position::new(0.0, 0.0), Goal::new(Position::new(20.0, 20.0), 0.5));

        let wall_list = vec![
            Wall {
                position_1: Position::new(5.0, 0.0),
                position_2: Position::new(5.0, 7.0),
            },
            Wall {
                position_1: Position::new(3.0, 10.0),
                position_2: Position::new(13.0, 10.0),
            },
        ];
        let mut position_list = vec![];
        for count in 1..1000 {
            position_list.push(node.position);
            node._move(0.05, &wall_list, &vec![]);
        }

        position_list.push(node.position);

        plot(&mut position_list, &wall_list, &vec![]);

        assert!((node.position.x - 20.0).abs() < 0.1);
        assert!((node.position.y - 20.0).abs() < 0.1);
    }

    #[test]
    fn test_move_with_other_node() {
        let goal = Goal::new(Position::new(20.0, 20.0), 0.5);
        let mut node = Node::new(Position::new(0.0, 0.0), goal);

        let node_list = vec![
            Node::new(Position::new(5.0, 5.05), goal),
            Node::new(Position::new(5.0, 5.15), goal),
            Node::new(Position::new(5.0, 5.25), goal),
            Node::new(Position::new(5.0, 5.35), goal),
            Node::new(Position::new(5.0, 5.45), goal),
        ];
        let mut position_list = vec![];
        for _ in 1..1000 {
            position_list.push(node.position);
            node._move(0.05, &vec![], &node_list);
        }

        position_list.push(node.position);

        plot(&mut position_list, &vec![], &node_list);

        assert!((node.position.x - 20.0).abs() < 0.1);
        assert!((node.position.y - 20.0).abs() < 0.1);
    }

    #[test]
    fn test_wall_distance1() {
        let n = Node::new(Position::new(0.0, 0.0), Goal::new(Position::new(0.0, 5.0), 0.5));
        let d = Wall {
            position_1: Position::new(5.0, -1.0),
            position_2: Position::new(5.0, 1.0),
        }
        .calc_distance_to_position_orthogonal(n.position);

        assert_eq!(d, 5.0);
    }

    #[test]
    fn test_wall_distance2() {
        let n = Node::new(Position::new(0.0, 0.0), Goal::new(Position::new(0.0, 5.0), 0.5));
        let d = Wall {
            position_1: Position::new(-100.0, 0.5),
            position_2: Position::new(100.0, 0.5),
        }
        .calc_distance_to_position_orthogonal(n.position);

        assert_eq!(d, 0.5);
    }

    #[test]
    fn test_wall_distance3() {
        let n = Node::new(Position::new(0.0, 0.0), Goal::new(Position::new(0.0, 5.0), 0.5));
        let d = Wall {
            position_1: Position::new(-2.0, -2.0),
            position_2: Position::new(2.0, 2.0),
        }
        .calc_distance_to_position_orthogonal(n.position);

        assert_eq!(d, 0.0);
    }

    #[test]
    fn test_othro1() {
        let n = Node::new(Position::new(0.0, 0.0), Goal::new(Position::new(0.0, 5.0), 0.5));
        let d = Wall {
            position_1: Position::new(-2.0, -2.0),
            position_2: Position::new(2.0, 2.0),
        }
        .calc_distance_to_position_orthogonal(n.position);

        assert_eq!(d, 0.0);
    }
}
