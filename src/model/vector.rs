use serde::{Deserialize, Serialize};
use std::ops;
use wasm_bindgen::prelude::*;

impl ops::Mul<f64> for Vector {
    type Output = Vector;

    fn mul(self, rhs: f64) -> Self::Output {
        Vector::new(self.x * rhs, self.y * rhs)
    }
}

impl ops::Sub<Vector> for Vector {
    type Output = Vector;

    fn sub(self, rhs: Vector) -> Self::Output {
        Vector::new(self.x - rhs.x, self.y - rhs.y)
    }
}
impl ops::Add<Vector> for Vector {
    type Output = Vector;

    fn add(self, rhs: Vector) -> Self::Output {
        Vector::new(self.x + rhs.x, self.y + rhs.y)
    }
}

impl ops::Div<f64> for Vector {
    type Output = Vector;

    fn div(self, rhs: f64) -> Self::Output {
        Vector::new(self.x / rhs, self.y / rhs)
    }
}

#[derive(Copy, Clone, Debug, Serialize, Deserialize)]
#[wasm_bindgen]
pub struct Vector {
    pub x: f64,
    pub y: f64,
}

#[wasm_bindgen]
impl Vector {
    pub fn new(x: f64, y: f64) -> Self {
        Vector { x, y }
    }

    pub fn distance(&self) -> f64 {
        let sum = self.x.powi(2) + self.y.powi(2);
        sum.sqrt()
    }

    pub fn e(&self) -> Vector {
        if self.distance() == 0.0 {
            return Vector::new(0.0, 0.0);
        }
        return Vector {
            x: self.x / self.distance(),
            y: self.y / self.distance(),
        };
    }

    pub fn orthogonal_vec(&self) -> Vector {
        return Vector::new(self.y, -self.x);
    }
}

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn test_vector_sub() {
        let a = Vector::new(10f64, 20f64) - Vector::new(5f64, 1f64);
        assert_eq!(a.x, 5f64);
        assert_eq!(a.y, 19f64);
    }
}
