

use super::position::Position;
use serde::{Deserialize, Serialize};
use wasm_bindgen::prelude::*;

use super::vector::Vector;

#[derive(Debug, Clone, Serialize, Deserialize)]
#[wasm_bindgen]
pub struct Wall {
    pub position_1: Position,
    pub position_2: Position,
}

#[wasm_bindgen]
impl Wall {
    pub fn new(position_1: Position, position_2: Position) -> Self {
        Wall {
            position_1,
            position_2,
        }
    }
    // 離れる方向の直角ベクトル
    pub fn outhogonal_vec(&self, position: Position) -> Vector {
        let ortho = (self.position_2 - self.position_1).orthogonal_vec();

        let e_micro = ortho.e() * 0.01;

        let _d = self.calc_distance_to_position_orthogonal(position + e_micro);
        let d = self.calc_distance_to_position_orthogonal(position);

        // 距離遠くなってるか
        if d < _d {
            return ortho;
        }
        return ortho * -1.0;
    }

    // 最も近い箇所からのベクトル
    pub fn min_vec(&self, position: Position) -> Vector {
        if self.is_intersection_in_line(position) {
            return self.outhogonal_vec(position);
        }

        let d1 = (self.position_1 - position).distance();
        let d2 = (self.position_2 - position).distance();

        if d1 < d2 {
            return position - self.position_1;
        }

        return position - self.position_2;
    }

    pub fn intersection(&self, position: Position) -> Position {
        let _vec = self.outhogonal_vec(position).e() * -1.0;
        let d = self.calc_distance_to_position_orthogonal(position);

        position + (_vec * d)
    }

    // ライン上かどうか
    pub fn is_intersection_in_line(&self, position: Position) -> bool {
        let intersection = self.intersection(position);

        if self.position_1.x == self.position_2.x {
            let is_in_a =
                self.position_1.y <= intersection.y && intersection.y <= self.position_2.y;
            let is_in_b =
                self.position_2.y <= intersection.y && intersection.y <= self.position_1.y;
            return is_in_a || is_in_b;
        }

        let is_in_a = self.position_1.x <= intersection.x && intersection.x <= self.position_2.x;
        let is_in_b = self.position_2.x <= intersection.x && intersection.x <= self.position_1.x;
        is_in_a || is_in_b
    }

    pub fn calc_distance_to_position_minimum(&self, position: Position) -> f64 {
        if self.is_intersection_in_line(position) {
            return self.calc_distance_to_position_orthogonal(position);
        }

        let d1 = (self.position_1 - position).distance();
        let d2 = (self.position_2 - position).distance();
        d1.min(d2)
    }

    pub fn calc_distance_to_position_orthogonal(&self, position: Position) -> f64 {
        let vec_wall = self.position_2 - self.position_1;

        let katamuki = vec_wall.y / vec_wall.x;

        if katamuki.is_infinite() {
            return (position.x - self.position_1.x).abs();
        }

        let b = self.position_1.y - katamuki * self.position_1.x;
        let d = ((katamuki * position.x) - position.y + b).abs() / (katamuki.powi(2) + 1.0).sqrt();

        d
    }
}

#[cfg(test)]
mod test {

    #[test]
    fn test_range() {
        assert_eq!((1.0..5.0).contains(&2.0), true);
        assert_eq!((5.0..1.0).contains(&2.0), false);
        assert_eq!((1.0..5.0).contains(&0.01), false);
        assert_eq!((1.0..5.0).contains(&1.01), true);
    }
}
