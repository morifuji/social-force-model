use super::position::Position;
use serde::{Deserialize, Serialize};
use wasm_bindgen::prelude::*;

#[derive(Debug, Clone, Serialize, Deserialize, Copy)]
#[wasm_bindgen]
pub struct Goal {
    pub position: Position,
    /** 半径 */
    pub r: f64,
}

#[wasm_bindgen]
impl Goal {
    pub fn new(position: Position, r: f64) -> Self{
        Goal {
            position,
            r
        }
    }
}