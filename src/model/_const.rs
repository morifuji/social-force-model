/** 時間 */
pub const RELAXATION_TIME: f64 = 0.5;
/** インタラクション係数(N) */
pub const INTERACTION_COEFFICIENT: f64 = 6f64;
/** 反発係数(m) */
pub const RESTITUTION_COEFFICIENT: f64 = 0.08;
