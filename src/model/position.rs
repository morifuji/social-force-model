use std::ops;

use super::vector::Vector;
use serde::{Deserialize, Serialize};
use wasm_bindgen::prelude::*;

#[derive(Copy, Clone, Debug, Serialize, Deserialize)]
#[wasm_bindgen]
pub struct Position {
    pub x: f64,
    pub y: f64,
}

#[wasm_bindgen]
impl Position {
    pub fn new(x: f64, y: f64) -> Self {
        Position { x, y }
    }
}

impl ops::Add<Position> for Position {
    type Output = Vector;

    fn add(self, _other: Position) -> Self::Output {
        Vector::new((self.x + _other.x) as f64, (self.y + _other.y) as f64)
    }
}
impl ops::Sub<Position> for Position {
    type Output = Vector;
    fn sub(self, _other: Position) -> Self::Output {
        let x = (self.x as f64).sub(_other.x as f64);
        let y = (self.y as f64).sub(_other.y as f64);
        Vector::new(x, y)
    }
}

impl ops::Add<Vector> for Position {
    type Output = Position;

    fn add(self, _other: Vector) -> Self::Output {
        let x = self.x.add(_other.x);
        let y = self.y.add(_other.y);
        Position::new(x, y)
    }
}

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn test_check() {
        let a = Position::new(10.0, 20.0) - Position::new(5.0, 1.0);
        assert_eq!(a.x, 5f64);
        assert_eq!(a.y, 19f64);
    }

    #[test]
    fn test_distance() {
        let a = Position::new(10.0, 20.0) - Position::new(7.0, 16.0);
        assert_eq!(a.distance(), 5f64);
    }

    #[test]
    fn test_distance_minus() {
        let a = Position::new(10.0, 20.0) - Position::new(14.0, 23.0);
        assert_eq!(a.distance(), 5f64);
    }

    #[test]
    fn test_e() {
        let a = Position::new(10.0, 20.0) - Position::new(7.0, 16.0);
        assert_eq!(a.e().x, 3f64 / 5f64);
        assert_eq!(a.e().y, 4f64 / 5f64);
    }
}
