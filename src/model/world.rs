use super::node::Node;

use super::wall::Wall;
extern crate web_sys;

use wasm_bindgen::prelude::*;

#[wasm_bindgen]
pub struct World {
    node_list: Vec<Node>,
    wall_list: Vec<Wall>,
    pub width: i16,
    pub height: i16,
}

impl World {
    pub fn new(
        node_list: Vec<Node>,
        wall_list: Vec<Wall>,
        world_width: i16,
        world_height: i16,
    ) -> World {
        World {
            node_list,
            wall_list,
            width: world_width,
            height: world_height,
        }
    }

    pub fn node_list(&self) -> &Vec<Node> {
        &self.node_list
    }
    pub fn wall_list(&self) -> &Vec<Wall> {
        &self.wall_list
    }
}

#[wasm_bindgen]
impl World {
    pub fn new_wasm(
        _node_list: JsValue,
        _wall_list: JsValue,
        world_width: i16,
        world_height: i16,
    ) -> World {
        let node_list: Vec<Node> = serde_wasm_bindgen::from_value(_node_list).unwrap();
        let wall_list: Vec<Wall> = serde_wasm_bindgen::from_value(_wall_list).unwrap();
        World::new(node_list, wall_list, world_width, world_height)
    }

    pub fn go_by(&mut self, duration: f64) {
        let _wall_list = self.wall_list.clone();
        let _node_list = self.node_list.clone();
        self.node_list.iter_mut().enumerate().for_each(|(i, node)| {
            node._move(duration, &_wall_list, &_node_list);
        });

        self.node_list.retain(|x| {
            !x.is_in_goal()
        });
    }

    pub fn _wall_list(&self) -> JsValue {
        JsValue::from_serde(&self.wall_list).unwrap()
    }

    pub fn _node_list(&self) -> JsValue {
        JsValue::from_serde(&self.node_list).unwrap()
    }
}

#[cfg(test)]
mod test {

    use crate::model::goal::Goal;

    use super::*;
    use super::super::position::Position;

    #[test]
    fn test_world() {
        let goal = Goal::new(Position::new(16.0, 16.0), 0.25);

        let node_list = vec![
            Node::new(Position::new(0.0, 1.0), goal),
            Node::new(Position::new(2.0, 3.0), goal),
            Node::new(Position::new(4.0, 2.0), goal),
        ];

        let mut a = World::new(node_list, vec![], 20, 20);

        for _ in 0..100 {
            a.go_by(0.5);
        }
    }
}
