use std::ops::Range;

use plotters::coord::types::RangedCoordf64;
use plotters::prelude::*;

use chrono::Local;
use plotters::prelude::*;

use crate::model::goal::Goal;
use crate::model::node::Node;
use crate::model::position::Position;
use crate::model::wall::Wall;

pub fn plot(
    data: &Vec<Position>,
    wall_list: &Vec<Wall>,
    node_list: &Vec<Node>,
) -> Result<(), Box<dyn std::error::Error>> {
    // wasmで代替できたためコメントアウト
    // let data_list = data.into_iter().map(|p| {
    //     (p.x, p.y)
    // });

    // let a = Local::now().timestamp();

    // let path = a.to_string() + ".png".as_ref();
    // let root = BitMapBackend::new(&path, (1280, 1280)).into_drawing_area();
    // root.fill(&WHITE)?;
    // let mut chart = ChartBuilder::on(&root)
    // .build_cartesian_2d(0.0f64..20.0f64, 0.0f64..20.0f64)?;

    // chart.configure_mesh().draw()?;

    // chart
    //     .draw_series(LineSeries::new(
    //         data_list.clone(),
    //         &RED,
    //     ))?;

    // chart.draw_series(PointSeries::of_element(
    //        data_list.enumerate().filter(|(i, p)| {
    //         return i%10 == 0;
    //        }),
    //         5,
    //         &RED,
    //         &|c, s, st| {
    //             return EmptyElement::at(c.1)    // We want to construct a composed element on-the-fly
    //             + Circle::new((0,0),s,st.filled()) // At this point, the new pixel coordinate is established
    //             // + Text::new(format!("{} ({},{})", 0.2*(c.0 as f64), c.1.0,c.1.1), (10, 0), ("sans-serif", 30).into_font());
    //             + Text::new(format!("{}", 0.2*(c.0 as f64)), (10, 0), ("sans-serif", 30).into_font());
    //         },
    //     ))?;

    //     for pedestrian in node_list {
    //         chart.draw_series(PointSeries::of_element(
    //             vec!((pedestrian.position.x, pedestrian.position.y)),
    //              5,
    //              &GREEN,
    //              &|c, s, st| {
    //                  EmptyElement::at(c)    // We want to construct a composed element on-the-fly
    //                  + Circle::new((0,0),s,st.filled()) // At this point, the new pixel coordinate is established;
    //              },
    //          ))?;
    //     }

    //     for wall in wall_list {
    //         chart.draw_series(LineSeries::new(
    //             vec!((wall.position_1.x, wall.position_1.y), (wall.position_2.x, wall.position_2.y)),
    //              &BLUE
    //          ))?;
    //     }

    // chart
    //     .configure_series_labels()
    //     .background_style(&WHITE.mix(0.8))
    //     .border_style(&BLACK)
    //     .draw()?;

    Ok(())
}

pub fn draw_node(
    chart: &mut ChartContext<BitMapBackend, Cartesian2d<RangedCoordf64, RangedCoordf64>>,
    node_list: &Vec<Node>,
) {
    for (i, pedestrian) in node_list.iter().enumerate() {
        chart
            .draw_series(PointSeries::of_element(
                vec![(pedestrian.position.x, pedestrian.position.y)],
                5,
                &get_color_by_index(i),
                &|c, s, st| {
                    EmptyElement::at(c)    // We want to construct a composed element on-the-fly
                 + Circle::new((0,0),s,st.filled()) // At this point, the new pixel coordinate is established;
                },
            ))
            .unwrap();
    }
}

pub fn draw_goal(
    chart: &mut ChartContext<BitMapBackend, Cartesian2d<RangedCoordf64, RangedCoordf64>>,
    goal: &Goal,
) {
    chart.draw_series(PointSeries::of_element(
            vec!((goal.position.x, goal.position.y)),
             15,
             &BLACK,
             &|c, s, st| {
                 EmptyElement::at(c)    // We want to construct a composed element on-the-fly
                 + Circle::new((0,0),s,st.filled())
                 + Text::new(format!("GOAL ({}, {})", c.0, c.1), (10, 0), ("sans-serif", 10).into_font())
             },
         )).unwrap();
}

pub fn draw_wall(
    chart: &mut ChartContext<BitMapBackend, Cartesian2d<RangedCoordf64, RangedCoordf64>>,
    wall_list: &Vec<Wall>,
) {
    for wall in wall_list {
        chart.draw_series(LineSeries::new(
            vec![
                (wall.position_1.x, wall.position_1.y),
                (wall.position_2.x, wall.position_2.y),
            ],
            &BLUE,
        )).unwrap();
    }
}

fn get_color_by_index(i: usize) -> RGBColor {
    return match i % 6 {
        0 => BLUE,
        1 => CYAN,
        2 => GREEN,
        3 => MAGENTA,
        4 => RED,
        5 => YELLOW,
        _ => panic!("can't decide color"),
    };
}
